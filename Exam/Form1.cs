﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Exam
{
    public partial class Form1 : Form
    {
        Database db = new Database();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            db.Connecter();
            FillDataGrid();
            GetTables();
            db.CON.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (numericUpDown1.Value.ToString() != "" || textBox1.Text != "" || textBox2.Text != "")
                {
                    // loop for exists rows
                    for (int i = 0; i < db.Set.Tables["Project"].Rows.Count; i++)
                    {
                        //string value = database.Set.Tables[0].Rows[i][0].ToString();
                        if (numericUpDown1.Value.ToString().Equals(dataGridView1.Rows[i].Cells[0].ToString()))
                        {
                            MessageBox.Show("Row is Exists !!!");
                        }
                        else
                        {
                            string query = $"insert into Project(ref,titre,description,statut,DCreation) values({numericUpDown1.Value},'{textBox1.Text}','{textBox2.Text}',{1},'{DateTime.Now}');";
                            db.CMD = new SqlCommand(query, db.CON);
                            db.Connecter();
                            db.CMD.ExecuteNonQuery();
                            MessageBox.Show("Insert is valid");
                            FillDataGrid();
                            db.CON.Close();
                        }
                    }
                    return;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void FillDataGrid()
        {
            try
            {
                string query = @"select * from Project;";
                db.CMD = new SqlCommand(query,db.CON);
                db.Reader = db.CMD.ExecuteReader();
                db.Table = new DataTable();
                db.Table.Load(db.Reader);
                dataGridView1.DataSource = db.Table;
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        private void GetTables()
        {
            try
            {
                if (db.Set.Tables["Project"] != null)
                {
                    db.Set.Tables["Project"].Clear();
                }
                db.Adapter = new SqlDataAdapter("select * from Project;", db.CON);
                db.Adapter.Fill(db.Set, "Project");
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
                numericUpDown1.Text = row.Cells[0].Value.ToString();
                textBox1.Text = row.Cells[1].Value.ToString();
                textBox2.Text = row.Cells[5].Value.ToString();
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            /*
            for (int i = 0; i < db.Set.Tables["Project"].Rows.Count; i++)
            {
                //string value = database.Set.Tables[0].Rows[i][0].ToString();
                if (textBox1.Text.Equals(db.Set.Tables["Project"].Rows[i][0].ToString()))
                {
                    db.Set.Tables["Project"].Rows[i][0] = numericUpDown1.Value;
                    db.Set.Tables["Project"].Rows[i][1] = textBox1.Text;
                    db.Set.Tables["Project"].Rows[i][4] = DateTime.Now;
                    db.Set.Tables["Project"].Rows[i][5] = textBox2.Text;
                    MessageBox.Show("Update Successfully");
                    dataGridView1.DataSource = db.Set.Tables["Project"];
                    return;
                }
            }
            */
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < db.Set.Tables["Project"].Rows.Count; i++)
                {
                    //string value = database.Set.Tables[0].Rows[i][0].ToString();
                    if (numericUpDown1.Value.ToString().Equals(db.Set.Tables["Project"].Rows[i][0].ToString()) && db.Set.Tables["Project"].Rows[i][3].ToString() != "2")
                    {
                        db.Set.Tables["Project"].Rows[i][2] = Convert.ToInt32(2);
                        MessageBox.Show("Update Successfully");
                        dataGridView1.DataSource = db.Set.Tables["Project"];
                        return;
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        
        private void get()
        {
             
        }
    }
}
