﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam
{
    class Project
    {
        private int refer;
		private string titre;
		private string description;
		private int statut;
		private DateTime dcreation;
		private DateTime dmodification;

        public int Refer { get => refer; set => refer = value; }
        public string Titre { get => titre; set => titre = value; }
        public string Description { get => description; set => description = value; }
        public int Statut { get => statut; set => statut = value; }
        public DateTime Dcreation { get => dcreation; set => dcreation = value; }
        public DateTime Dmodification { get => dmodification; set => dmodification = value; }

        public Project(int refer, string titre, string description, int statut, DateTime dcreation, DateTime dmodification)
        {
            this.refer = refer;
            this.titre = titre;
            this.description = description;
            this.statut = statut;
            this.dcreation = dcreation;
            this.dmodification = dmodification;
        }
    }
}
