﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Exam
{
    class Database
    {
        public SqlConnection CON = new SqlConnection(@"Data Source=DESKTOP-J5SB1HI;Initial Catalog=Exam;Integrated Security=True");
        public SqlCommand CMD = new SqlCommand();
        public SqlDataReader Reader;
        public SqlDataAdapter Adapter = new SqlDataAdapter();
        public DataSet Set = new DataSet();
        public DataTable Table = new DataTable();
        public DataRow Row;
        public SqlCommandBuilder Builder;

        public void Connecter()
        {
            if(CON.State.Equals(ConnectionState.Closed) || CON.State.Equals(ConnectionState.Broken))
            {
                CON.Open();
                return;
            }
        }
    }
}
